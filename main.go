package main

import (
	"context"
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/go-sql-driver/mysql"
	"github.com/joho/sqltocsv"
)

var db *sql.DB

var server = "localhost"

//var port = 1433
var user = "SA"
var password = "pa$$word01"
var database = "Ficoms_Selection"

// var server = "10.0.0.49"

// //var port = 1433
// var user = "uservaldo"
// var password = "vx@u53rv4ld0"
// var database = "Ficoms_Selection"

func ConnectDB() (*sql.DB, error) {
	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;database=%s;",
		server, user, password, database)
	var err error
	// Create connection pool
	db, err = sql.Open("sqlserver", connString)
	if err != nil {
		log.Fatal("Error creating connection pool: ", err.Error())
		return nil, err
	}
	ctx := context.Background()
	err = db.PingContext(ctx)
	if err != nil {
		log.Fatal(err.Error())
	}
	fmt.Printf("Connected!\n")

	return db, nil
}

func main() {
	//runtime.GOMAXPROCS(2)

	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;database=%s;",
		server, user, password, database)
	var err error
	// Create connection pool
	db, err = sql.Open("sqlserver", connString)
	if err != nil {
		log.Fatal("Error creating connection pool: ", err.Error())
		return
	}
	ctx := context.Background()
	err = db.PingContext(ctx)
	if err != nil {
		log.Fatal(err.Error())
	}
	fmt.Printf("Connected!\n")

	createCSV()

	set_truncate()

	now := time.Now()
	path_name := now.Format("2006-01-02")

	path2 := "/home/valdo/uploads/" + path_name + "/"

	files, err := ioutil.ReadDir(path2)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for _, f := range files {
		//fmt.Println(f.Name())
		os.Chmod(f.Name(), 777)

		file_name := f.Name()
		file_path := path2 + "/" + file_name
		mysql.RegisterLocalFile(file_path)

		upload_inventory(file_path)

		upload_master(file_path)

		upload_master_log(file_path)

	}

	fmt.Println("upload success")

}

func createCSV() {
	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;database=%s;",
		server, user, password, database)
	var err error
	// Create connection pool
	db, err = sql.Open("sqlserver", connString)
	if err != nil {
		log.Fatal("Error creating connection pool: ", err.Error())
		return
	}
	ctx := context.Background()

	tsql := fmt.Sprintf(`SELECT DISTINCT (AgreementNo) as AgreementNo,FullName,OVD,Bucket,Tenor,DueDate,AngsuranKe,OSBalance,
	AmountDue,Denda,Deposit,Region,Branch,POSName,ResidenceCity,ResidenceKecamatan,ResidenceKelurahan,
	ResidenceZipCode,ResidenceAddress,CompanyPhone,ResidencePhone,MobilePhone,EmergencyPhone,YearSales,
	MonthSales,CMO,CollectorID,DealerName,Category,Product,AOID,SurveyorId,CAID,Brand,Model,SerialNo1,
	NoPolisi,Warna,BirthPlaceDate,Gender,MaritalStatus,UMUR,IbuKandung,StatusRumah,Pekerjaan,Bagian,
	Jabatan,LamaKerja,TotalIncome,EmergencyContactName,EMRCONTACTRELATIONS,LegalAddress,LegalKelurahan,
	LegalKecamatan,LegalCity,AssetDescription,NoTelpBaru FROM ValdoExport;`)

	rows, err := db.QueryContext(ctx, tsql)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	defer rows.Close()

	csvconverter := sqltocsv.New(rows)
	csvconverter.Delimiter = '|'
	now := time.Now()
	path_name := now.Format("2006-01-02")
	os.Mkdir(string("/home/valdo/uploads/"+path_name), 0777)

	filename := "upload_" + path_name + ".csv"

	// csvconverter.WriteFile(path_name + "/" + filename)

	csvconverter.WriteFile("/home/valdo/uploads/" + path_name + "/" + filename) // live

	os.Chmod(string("/home/valdo/uploads/"+path_name+"/"+filename), 0777)

}

func ConnectMysql() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:pa$$word01@tcp(127.0.0.1:3306)/finansia_beta")
	if err != nil {
		return nil, err
	}

	return db, nil
}

func upload_master(file string) {
	//now := time.Now()

	// date := now.Format("2006-01-02 15:04:05")
	//date := now.Format("2006-01-02")

	db, err := ConnectMysql()

	if err != nil {
		log.Fatalln("connect error : " + err.Error())
	}

	defer db.Close()

	var sql string
	sql += "LOAD DATA LOCAL INFILE '" + file + "' INTO TABLE hdr_debtor_main FIELDS TERMINATED BY '|'  IGNORE 1 LINES "
	sql += "(primary_1,name,ovd,bucket,@Tenor,due_date,angsuran_ke,balance,amount_due,@Denda,@DePosit,region,branch, "
	sql += "pos_name,@Residence_City,@Residence_Kecamatan,@Residence_Kelurahan,@Kode_Pos,@Residence_Address,office_phone1,"
	sql += "home_phone1,cell_phone1,office_phone2,@Year_Sales,@Month_sales,@CMO,@Collector,@Dealer,category,product,@AOID,@Surveyor_Id,@CAID,@Brand,@Model_,"
	sql += "@No_Rangka,@No_Polisi,@warna,@Birth_Place,@Gender,@Marital_Satus,@Umur,@Ibu_Kandung,"
	sql += "@Status_Rumah,@Pekerjaan,@Bagian,@Jabatan,@Lama_Kerja,@Total_Income,@EMR_Relationship,@EMR_Contact,@Legal_Address,@Legal_lurah,@Legal_camat,"
	sql += "@Legal_city,@asset_desc,cell_phone2) "

	_, err = db.Exec(sql)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

}

func upload_master_log(file string) {
	// now := time.Now()

	// date := now.Format("2006-01-02 15:04:05")

	db, err := ConnectMysql()

	if err != nil {
		log.Fatalln("connect error : " + err.Error())
	}

	defer db.Close()

	sql := "LOAD DATA LOCAL INFILE '" + file + "' INTO TABLE hdr_tmp_log IGNORE 1 LINES (VALUE) SET primary_1 = SUBSTRING_INDEX(VALUE, '|', 1), createdate = CURRENT_TIMESTAMP;"
	_, err = db.Exec(sql)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

}

func upload_inventory(file string) {
	db, err := ConnectMysql()

	if err != nil {
		log.Fatalln("connect error : " + err.Error())
	}

	defer db.Close()

	sql := `LOAD DATA LOCAL INFILE '` + file + `' INTO TABLE valdoexport FIELDS TERMINATED BY '|' IGNORE 1 LINES (AgreementNo,FullName,OVD,Bucket,Tenor,DueDate,AngsuranKe,OSBalance,
		AmountDue,Denda,Deposit,Region,Branch,POSName,ResidenceCity,ResidenceKecamatan,ResidenceKelurahan,
		ResidenceZipCode,ResidenceAddress,CompanyPhone,ResidencePhone,MobilePhone,EmergencyPhone,YearSales,
		MonthSales,CMO,CollectorID,DealerName,Category,Product,AOID,SurveyorId,CAID,Brand,Model,SerialNo1,
		NoPolisi,Warna,BirthPlaceDate,Gender,MaritalStatus,UMUR,IbuKandung,StatusRumah,Pekerjaan,Bagian,
		Jabatan,LamaKerja,TotalIncome,EmergencyContactName,EMRCONTACTRELATIONS,LegalAddress,LegalKelurahan,
		LegalKecamatan,LegalCity,AssetDescription,NoTelpBaru);`
	_, err = db.Exec(sql)

	if err != nil {
		fmt.Println(err.Error())
		return
	}
}

func set_truncate() {
	db, err := ConnectMysql()

	if err != nil {
		log.Fatalln("connect error : " + err.Error())
	}

	defer db.Close()

	var sql = "TRUNCATE hdr_debtor_main"
	_, err = db.Exec(sql)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	var sql1 = "TRUNCATE hdr_tmp_log"
	_, err = db.Exec(sql1)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

}
