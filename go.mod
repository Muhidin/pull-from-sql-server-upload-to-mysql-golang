module write-csv-mssql

go 1.16

require (
	github.com/denisenkom/go-mssqldb v0.10.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/joho/sqltocsv v0.0.0-20210428211105-a6d6801d59df
)
